import random
from datetime import date

from django.db.models import Q
from django.shortcuts import render,redirect
from .models import *
from django.contrib.auth import authenticate, logout, login

# Create your views here.
def index(request):
    try:
        if request.method == 'POST':
            name = request.POST['name']
            emailid = request.POST['emailid']
            contact = request.POST['contact']
            message = request.POST['message']
            try:
                Contact.objects.create(name=name, contact=contact, emailid=emailid,message=message, enquiryDate=date.today(),isread="No")
                error = "no"
            except:
                error = "yes"
    except:
        rate = None
        if request.method == "POST":
            lamount = request.POST['lamount']
            lrate = request.POST['lrate']
            tenure = request.POST['tenure']

            rate = float(lrate) / 12  # converting int to month
            time = int(tenure) * 12  # Converting in to month
            simpleinterst = rate * time * int(lamount) / 100;

            totalpayable = simpleinterst + int(lamount)
            monthlyEmi = round(totalpayable / time, 2)
    return render(request, 'index.html', locals())

# ================ Admin Here ====================

def adminlogin(request):
    error = ""
    if request.method == 'POST':
        u = request.POST['uname']
        p = request.POST['pwd']
        user = authenticate(username=u, password=p)
        try:
            if user.is_staff:
                login(request, user)
                error = "no"
            else:
                error = "yes"
        except:
            error = "yes"
    return render(request, 'adminlogin.html', locals())

def dashboard(request):
    if not request.user.is_authenticated:
        return redirect('admin_login')
    totalreguser = Signup.objects.all().count()
    totalnewloan = Application.objects.filter(Status__isnull=True).count()
    totalapprovedloan = Application.objects.filter(Status="Approved").count()
    totalrejectloan = Application.objects.filter(Status="Rejected").count()
    totaldisbursedloan = Application.objects.filter(Status="Disbursed").count()
    totalunread = Contact.objects.filter(isread="No").count()
    totalread = Contact.objects.filter(isread="yes").count()
    return render(request, 'admin/dashboard.html', locals())

def newLoan_req(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.filter(Status__isnull=True)
    return render(request, 'admin/newLoan_req.html', locals())

def allLoan_req(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.all()
    return render(request, 'admin/allLoan_req.html', locals())

def rejectLoan_req(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.filter(Status="Rejected")
    return render(request, 'admin/rejectLoan_req.html', locals())

def adminViewloanReqDetails(request,pid):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.get(id=pid)
    appreport = Applicationtracking.objects.filter(application=application)

    reportcount = Applicationtracking.objects.filter(application=application).count()

    if request.method == "POST":
        if request.method == "POST":
            status = request.POST['Status']
            remark = request.POST['Remark']

            try:
                apptracking = Applicationtracking.objects.create(application=application, Remark=remark, Status=status)
                application.Status = status
                application.Remark = remark
                application.save()
                error = "no"
            except:
                error = "yes"
    return render(request, 'admin/adminViewloanReqDetails.html', locals())

def newRequest(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.filter(Status='Approved')
    return render(request, 'admin/newRequest.html', locals())

def completeRequest(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.filter(Status='Disbursed')
    return render(request, 'admin/completeRequest.html', locals())
def viewDisbursedReqDetails(request,pid):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    application = Application.objects.get(id=pid)
    appreport = Applicationtracking.objects.filter(application=application)

    reportcount = Applicationtracking.objects.filter(application=application).count()

    try:
        rate = float(application.RateofInterest)/12  #converting int to month
        time = int(application.DTenure)*12       #Converting in to month
        simpleinterst = rate * time * int(application.LoanamountDisbursed) / 100;

        totalpayable =simpleinterst + int(application.LoanamountDisbursed)
        monthlyEmi = round(totalpayable/time,2)
    except:
        pass

    if request.method == "POST":
        LoanamountDisbursed =request.POST['LoanamountDisbursed']
        RateofInterest = request.POST['RateofInterest']
        DTenure = request.POST['DTenure']
        remark = request.POST['Remark']

        try:
            apptracking = Applicationtracking.objects.create(application=application, Remark=remark, Status="Disbursed")
            application.LoanamountDisbursed = LoanamountDisbursed
            application.RateofInterest = RateofInterest
            application.DTenure = DTenure
            application.Status = 'Disbursed'
            application.Remark = remark
            application.save()
            error = "no"
        except:
            error = "yes"
    return render(request, 'admin/viewDisbursedReqDetails.html', locals())

def betweendateReport(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    if request.method == "POST":
        fd = request.POST['FromDate']
        td = request.POST['ToDate']
        application = Application.objects.filter(Q(SubmitDate__gte=fd) & Q(SubmitDate__lte=td))
        return render(request, 'admin/reportbtwdates.html', locals())
    return render(request, 'admin/betweendateReport.html')

def unread_Enquiry(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    contact = Contact.objects.filter(isread="No")
    return render(request, 'admin/unread_Enquiry.html', locals())

def read_Enquiry(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    contact = Contact.objects.filter(isread="yes")
    return render(request, 'admin/read_Enquiry.html', locals())

def viewContactDetails(request,pid):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    contact = Contact.objects.get(id=pid)
    contact.isread = "yes"
    contact.save()
    return render(request, 'admin/viewContactDetails.html', locals())

def search(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    sd = None
    signup = None
    if request.method == 'POST':
        sd = request.POST['searchdata']
    try:
        signup = Signup.objects.get(MobileNumber=sd)
        application = Application.objects.filter(signup=signup)
    except:
        application = Application.objects.filter(ApplicationNumber=sd)
    return render(request, 'admin/search.html', locals())

def regUser(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    reguser = Signup.objects.all()
    return render(request, 'admin/regUser.html', locals())

def deleteUser(request,pid):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    User.objects.get(id=pid).delete()
    return redirect('regUser')

def adminChangePassword(request):
    if not request.user.is_authenticated:
        return redirect('adminlogin')
    error = ""
    user = request.user
    if request.method == "POST":
        o = request.POST['oldpassword']
        n = request.POST['newpassword']
        try:
            u = User.objects.get(id=request.user.id)
            if user.check_password(o):
                u.set_password(n)
                u.save()
                error = "no"
            else:
                error = 'not'
        except:
            error = "yes"
    return render(request, 'admin/adminChangePassword.html', locals())

def Logout(request):
    logout(request)
    return redirect('index')
# ====================  User ====================================

def signup(request):
    error =""
    if request.method == 'POST':
        fname = request.POST['firstName']
        lname = request.POST['lastName']
        mob = request.POST['MobileNumber']
        emailid = request.POST['emailid']
        pwd = request.POST['password']

        try:
            user = User.objects.create_user(username=emailid, password=pwd, first_name=fname, last_name=lname)
            Signup.objects.create(user=user, MobileNumber=mob)
            error = "no"
        except:
            error = "yes"
    return render(request, 'signup.html', locals())

def userlogin(request):
    error =""
    if request.method == 'POST':
        e = request.POST['email']
        p = request.POST['password']
        user = authenticate(username=e, password=p)
        try:
            if user:
                login(request, user)
                error = "no"
            else:
                error = "yes"
        except:
            error = "yes"
    return render(request, 'userlogin.html', locals())

def userDashboard(request):
    if not request.user.is_authenticated:
        return redirect('userlogin')

    user = User.objects.get(id=request.user.id)
    signup = Signup.objects.get(user=user)

    rate = None
    if request.method == "POST":
        lamount =request.POST['lamount']
        lrate = request.POST['lrate']
        tenure = request.POST['tenure']


        rate = float(lrate) / 12  # converting int to month
        time = int(tenure) * 12  # Converting in to month
        simpleinterst = rate * time * int(lamount) / 100;

        totalpayable = simpleinterst + int(lamount)
        monthlyEmi = round(totalpayable / time, 2)
    return render(request, 'user/userDashboard.html', locals())


def applicationForm(request):
    if not request.user.is_authenticated:
        return redirect('userlogin')

    user = User.objects.get(id=request.user.id)
    signup = Signup.objects.get(user=user)

    error = ""
    if request.method == "POST":
        applicationNo = str(random.randint(10000000, 99999999))
        PanNumber = request.POST['PanNumber']
        PanCardCopy = request.FILES['PanCardCopy']
        Address = request.POST['Address']
        AddressProofType = request.POST['AddressProofType']
        AddressDoc = request.FILES['AddressDoc']
        ServiceType = request.POST['ServiceType']
        MontlyIncome = request.POST['MontlyIncome']
        ExistingLoan = request.POST['ExistingLoan']
        ExpectedLoanAmount = request.POST['ExpectedLoanAmount']
        ProfilePic = request.FILES['ProfilePic']
        Tenure = request.POST['Tenure']
        GName = request.POST['GName']
        Gmobnum = request.POST['Gmobnum']
        Gemail = request.POST['Gemail']
        Gaddress = request.POST['Gaddress']

        try:
            Application.objects.create(signup=signup, ApplicationNumber=applicationNo, PanNumber=PanNumber, PanCardCopy=PanCardCopy,
                                       Address=Address, AddressProofType=AddressProofType, AddressDoc=AddressDoc, ServiceType=ServiceType,
                                       MontlyIncome=MontlyIncome, ExistingLoan=ExistingLoan, ExpectedLoanAmount=ExpectedLoanAmount, ProfilePic=ProfilePic,
                                       Tenure=Tenure, GName=GName, Gmobnum=Gmobnum, Gemail=Gemail, Gaddress=Gaddress, SubmitDate=date.today())
            error = "no"
        except:
            error = "yes"
    return render(request, 'user/applicationForm.html', locals())

def applicationHistory(request):
    if not request.user.is_authenticated:
        return redirect('userlogin')
    signup = Signup.objects.get(user=request.user)
    application = Application.objects.filter(signup=signup)
    return render(request, 'user/applicationHistory.html', locals())

def viewloanRequestDetails(request,pid):
    if not request.user.is_authenticated:
        return redirect('userlogin')
    application = Application.objects.get(id=pid)
    appreport = Applicationtracking.objects.filter(application=application)
    reportcount = Applicationtracking.objects.filter(application=application).count()

    try:
        rate = float(application.RateofInterest) / 12  # converting int to month
        time = int(application.DTenure) * 12  # Converting in to month
        simpleinterst = rate * time * int(application.LoanamountDisbursed) / 100;

        totalpayable = simpleinterst + int(application.LoanamountDisbursed)
        monthlyEmi = round(totalpayable / time, 2)
    except:
        pass

    return render(request, 'user/viewloanRequestDetails.html', locals())

def changePassword(request):
    if not request.user.is_authenticated:
        return redirect('userlogin')
    if not request.user.is_authenticated:
        return redirect('employees')
    error = ""
    user = request.user
    if request.method == "POST":
        o = request.POST['oldpassword']
        n = request.POST['newpassword']
        try:
            u = User.objects.get(id=request.user.id)
            if user.check_password(o):
                u.set_password(n)
                u.save()
                error = "no"
            else:
                error = 'not'
        except:
            error = "yes"
    return render(request, 'user/changePassword.html', locals())

